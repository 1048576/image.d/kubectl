#!/usr/bin/env sh

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

_sh "chown 65534:65534 ./"

# Install bash
_sh "apk add --no-cache bash"

# Install openssh
_sh "apk add --no-cache openssh"

# Install kubectl
_sh "apk add --no-cache --virtual .build-deps curl"
_sh "curl -LO https://storage.googleapis.com/kubernetes-release/release/\$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl"
_sh "chmod +x ./kubectl"
_sh "mv ./kubectl /usr/local/bin/kubectl"
_sh "kubectl version --client"

# Post install
_sh "apk del .build-deps"
_sh "rm $0"
